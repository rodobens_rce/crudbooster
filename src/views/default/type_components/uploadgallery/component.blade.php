<div class='form-group {{$header_group_class}} {{ ($errors->first($name))?"has-error":"" }}' id='form-group-{{$name}}' style="{{@$form['style']}}">
    <label class='control-label col-sm-2'>
        {{$form['label']}}
        @if($required)
        <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
        @endif
    </label>
    
            
    <div class="{{$col_width?:'col-sm-10'}}" class="input-group">
        <span  class="input-group-btn" >
            <button type="button" class="btn btn-danger clear"  data-type="minus" data-field="{{$name}}">
                <span class="glyphicon glyphicon-minus"></span>
            </button>
        </span>
        <input type='text' title="{{$form['label']}}" class='form-control'
        name="{{$name}}" id="{{@$form['id']}}" data-preview="{{@$form['datapreview']}}" data-input="{{@$form['datainput']}}" data="{{@$value}}" readonly/>
        
        <div class="text-danger">{!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}</div>
        <p class='help-block'>{{ @$form['help'] }}</p>
    </div>
    
    <div id="view{{@$form['id']}}" class="col-sm-8" style="transform:translateX(50%)">
        @switch($form['acceptedFiles'])
            @case('.pdf')   
                <embed width="0" height="0" id="{{@$form['datapreview']}}" type='application/pdf'>
                @break
            @case('.mp3')   
                <embed width="0" height="0" id="{{@$form['datapreview']}}" type='application/mp3'>
                @break
            @case('.mp4')
                <video width="0" height="0" controls="controls">
                    <source id="{{@$form['datapreview']}}" type='video/mp4'>
                </video>
                @break
            @default()
                <img id="{{@$form['datapreview']}}"  >
                @break
        @endswitch
    </div>
</div>
        
    @push('bottom')
    <script>
            $(document).ready(function(){

                $("#{{$form['id']}}").midia({
                    base_url: '{{url('')}}',
                    dropzone: {
                        acceptedFiles: "{{$form['acceptedFiles']}}" ,
                        maxFilesize: 30
                    },
                    directory_name: "{{$form['directory_name']}}",
                });
                
                @if($value)
                    @switch($form['acceptedFiles'])
                        @case('.pdf')      
                            $("#{{@$form['datapreview']}}").attr("src","{{$value}}").attr('width', 600).attr('height', 400);  
                            $("#{{@$form['id']}}").val("{{$value}}");
                            @break
                        @case('.mp3')      
                            $("#{{@$form['datapreview']}}").attr("data","{{$value}}");  
                            $("#{{@$form['id']}}").val("{{$value}}");
                            @break
                        @case('.mp4')
                            $("#{{@$form['datapreview']}}").attr("data","{{basename($value)}}");  
                            $("#{{@$form['id']}}").val("{{$value}}");
                            @break
                        @default()
                            $("#{{@$form['datapreview']}}").attr("src","{{'/midiautilizada/thumbs-250/'.basename($value)}}");  
                            $("#{{@$form['id']}}").val("{{$value}}");
                            @break
                    @endswitch
                 @endif
           });
            
           $('.clear').click(function(e){
                type= $(this).attr('data-type');
                fieldName = $(this).attr('data-field');
                var input = $("input[name='"+fieldName+"']");
                if(type == 'minus') {
                  input.val('').change();                  
                  input.parent().parent().find('#{{@$form['datapreview']}}').prop('src','').attr('width', 0).attr('height', 0);
                }else{
                    e.preventDefault();
                }    
            });
    </script>
    @endpush